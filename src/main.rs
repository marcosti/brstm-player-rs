use std::{time::Duration, thread, error::Error};

use crate::{player_thread::{PlayerThread, RequestType}, ui::PlayerUI};
mod ui;
mod player_thread;

fn main() -> Result<(), Box<dyn Error>> {
    let mut quit = false;

    let thread = PlayerThread::spawn("./assets/DontDoIt.brstm".to_string())?;
    println!("Player initialized");

    println!("{:?}", thread.send(RequestType::GetDuration)?.recv()?);

    let mut ui = PlayerUI::init(&thread)?;

    while !quit {
        // thread::sleep(Duration::from_secs(1));
        // let timepos = thread.send(RequestType::GetTimePos)?;
        // println!("{:?}",timepos.recv());
        ui.update_percent()?;
        ui.render_frame()?;
    }

    Ok(())
}
