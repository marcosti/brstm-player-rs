use std::{error::Error, io::{stdout, Stdout}};

use crossterm::{
    event::{self, Event, KeyCode, KeyEvent, KeyEventKind},
    execute,
    terminal::{
        disable_raw_mode, enable_raw_mode, size, window_size, EnterAlternateScreen, LeaveAlternateScreen, WindowSize
    },
};
use ratatui::{
    backend::CrosstermBackend,
    prelude::*,
    symbols::border,
    terminal::Terminal,
    widgets::{block::*, *},
};

use crate::player_thread::{PlayerThread, RequestType, ResponseType};

struct UIWidget {
    percent: f64
}
impl Widget for &UIWidget {
    fn render(self, area: Rect, buf: &mut Buffer)
        where
            Self: Sized {
        let block = Block::default()
            .title(Title::from("BRSTM Player".bold()))
            .borders(Borders::ALL)
            .border_set(border::THICK);

        LineGauge::default()
            .block(block)
            .ratio(self.percent)
            .line_set(symbols::line::THICK)
            .gauge_style(Style::new().fg(Color::LightRed).bg(Color::Gray))
            .render(area, buf);
    }
}

pub struct PlayerUI<'a> {
    percent: f64,
    time: f64,
    duration: f64,
    loop_flag: bool,
    terminal: Terminal<CrosstermBackend<Stdout>>,
    thread: &'a PlayerThread,
    window_size: WindowSize,
}

impl PlayerUI<'_> {
    pub fn init<'a>(thread: &'a PlayerThread) -> Result<PlayerUI<'a>, Box<dyn Error>> {
        execute!(stdout(), EnterAlternateScreen)?;
        enable_raw_mode()?;
        let terminal = Terminal::new(CrosstermBackend::new(stdout()))?;

        let percent = 0.0;
        let time = 0.0;
        let ResponseType::Duration(duration) = thread.send(RequestType::GetDuration)?.recv()? else {
            return Err("Error: Could not get duration".into());
        };
        // GetLoop is not implemented yet
        let loop_flag = true;
        // let ResponseType::Loop(loop_flag) = thread.send(RequestType::GetLoop)?.recv()? else {
        //     return Err("Error: Could not get loop".into());
        // };
        let window_size = window_size()?;

        Ok(PlayerUI { percent, time, duration, loop_flag, terminal, thread, window_size })
    }
    pub fn update_size<'a>(&'a mut self) -> Result<(), Box<dyn Error>> {
        self.window_size = window_size()?;
        Ok(())
    }
    pub fn update_time<'a>(&'a mut self) -> Result<(), Box<dyn Error>> {
        let ResponseType::TimePos(time) = self.thread.send(RequestType::GetTimePos)?.recv()? else {
            return Err("Error: Could not get time".into());
        };
        self.time = time;
        Ok(())
    }
    pub fn update_percent<'a>(&'a mut self) -> Result<(), Box<dyn Error>> {
        let ResponseType::PercentagePos(percent) = self.thread.send(RequestType::GetPercentTimePos)?.recv()? else {
            return Err("Error: Could not get time".into());
        };
        self.percent = percent as f64 / 100.0;
        Ok(())
    }
    pub fn render_frame<'a>(&'a mut self) -> Result<(), Box<dyn Error>> {
        self.terminal.draw(|f| {
            f.render_widget(&UIWidget { percent: self.percent.clone() }, f.size());
        });
        Ok(())
    }
}

